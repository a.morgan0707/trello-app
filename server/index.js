// system
require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

// modules
const authRoutes = require('./modules/auth');

const PORT = process.env.PORT || 3000;
const app = express();
app.use(cors());

app.use(express.json());
app.use('/auth', authRoutes);

async function start() {
    try {

        await mongoose.connect(process.env.DB_URL, { useNewUrlParser: true });

        app.listen(PORT, () => {
            console.log(`server was started on port ${PORT}`);
        })
    } catch (e) {
        console.log('Some error', e);
    }
}

start();
