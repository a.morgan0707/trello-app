const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../models/Users');
const { secretKey } = require('../utils/config');
const { validationResult } = require("express-validator");

function generateWebToken(id, username) {
    const payload = { id, username };

    return jwt.sign(payload, secretKey, { expiresIn: '24h' });
}

class AuthController {

    constructor() { }

    async login(request, response) {
        try {
            const { username, password } = request.body;
            const user = await User.findOne({ username });

            if (!user) {
                return response.status(400).json({ message: `User ${username} not found` });
            }

            const validPass = bcrypt.compareSync(password, user.password);

            if (!validPass) {
                return response.status(400).json({ message: 'Password is not correct' });
            }

            const token = generateWebToken(user._id, username);

            return response.json({ token });


        } catch (e) {
            console.log(e);
            response.status(400).json({ message: 'Login error' })
        }
    }

    async register(request, response) {
        try {
            const errors = validationResult(request);

            if (!errors.isEmpty()) {
                return response.status(400).json({ message: 'Registered error', errors });
            }

            const { username, password } = request.body;
            const candidate = await User.findOne({ username });



            if (candidate) {
                return response.status(400).json({ message: 'User was created' });
            }

            const hashPass = bcrypt.hashSync(password, 7);
            const user = new User({ username, password: hashPass });

            await user.save();

            return response.json({ message: `User - ${username} was created` });

        } catch (e) {
            console.log(e);
            response.status(400).json({ message: 'Registration error' })
        }
    }

    async testServer(request, response) {
        try {
            response.json('server working')
        } catch (e) {

        }
    }

}

module.exports = new AuthController();
