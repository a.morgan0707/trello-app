const { Router } = require("express");
const { check } = require('express-validator');
const router = Router();

const authController = require('../controlers/Auth-controller');

// auth endpoints
router.post('/login', authController.login);

router.get('/test', authController.testServer);

router.post(
    '/register',
    [
        check('username', 'User name is required').notEmpty(),
        check('password', 'Password cannot be shorter than 4 characters and longer than 10').isLength({ min: 4, max: 10 })
    ],
    authController.register
);


module.exports = router;
