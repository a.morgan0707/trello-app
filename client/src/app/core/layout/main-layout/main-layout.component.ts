import { Component, OnInit } from '@angular/core';
import { LocalstorageService } from "../../../shared/services/localstorage.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit {

  constructor(
    private localstorageService: LocalstorageService,
    private router: Router
  ) { }

  ngOnInit(): void {
    const token = this.localstorageService.get('token');

    if (token) {
      this.router.navigate(['/dashboard']);
    }
  }

}
