import { NgModule } from "@angular/core";
import { HeaderComponent } from "./header/header.component";
import { FooterComponent } from "./footer/footer.component";
import { MainLayoutComponent } from "./main-layout/main-layout.component";
import { RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [MainLayoutComponent, HeaderComponent, FooterComponent],
  declarations: [HeaderComponent, FooterComponent, MainLayoutComponent]
})
export class LayoutModule {}
