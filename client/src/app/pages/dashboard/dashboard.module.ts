import { NgModule } from "@angular/core";
import { DashboardComponent } from "./dashboard.component";

@NgModule({
  imports: [],
  exports: [],
  declarations: [DashboardComponent]
})
export class DashboardModule {}
