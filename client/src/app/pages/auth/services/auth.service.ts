import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../../environments/environment";
import { Observable } from "rxjs";
import { IFormData } from "../interfaces/form-data.interfaces";
import { IAuthResponseData } from "../interfaces/auth-response-data.interface";

@Injectable()
export class AuthService {
  apiUrl: string = environment.apiUrl;

  constructor(
    private http: HttpClient,
  ) {

  }

  public loginUser(userData: IFormData): Observable<IAuthResponseData> {
    return this.http.post<IAuthResponseData>(`${this.apiUrl}auth/login`, userData);
  }

  public registerUser(userData: IFormData): Observable<string> {
    return this.http.post<string>(`${this.apiUrl}auth/login`, userData);
  }
}
