import { Component, OnInit } from '@angular/core';
import { FormTypesEnum } from "./enum/form-types.enum";
import { IFormData } from "./interfaces/form-data.interfaces";
import { AuthService } from "./services/auth.service";
import { LocalstorageService } from "../../shared/services/localstorage.service";
import { IAuthResponseData } from "./interfaces/auth-response-data.interface";
import { Router } from "@angular/router";

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private localstorageService: LocalstorageService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  public onSubmit(data: { type: string, formData: IFormData }): void {
      data.type === FormTypesEnum.LoginForm ? this.login(data.formData) : this.register(data.formData);
  }

  private login(data: IFormData): void {
    this.authService.loginUser(data).subscribe((res: IAuthResponseData) => {
      this.router.navigate(['/dashboard']);
    })
  }

  private register(data: IFormData): void {
    this.authService.loginUser(data).subscribe((res: IAuthResponseData) => {
      this.router.navigate(['/dashboard']);
    })
  }
}
