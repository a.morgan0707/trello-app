import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { IFormData } from "../../interfaces/form-data.interfaces";
import { FormTypesEnum } from "../../enum/form-types.enum";
import { AuthService } from "../../services/auth.service";

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  private fromType: string = FormTypesEnum.LoginForm;

  @Output() formData: EventEmitter<{ type: string, formData: IFormData }> = new EventEmitter<{ type: string, formData: IFormData }>();

  public passwordVisible: boolean = false;
  public inputValue: string | null = null;
  public form: FormGroup = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', [Validators.required, Validators.max(10), Validators.min(4)])
  });

  constructor(
    private fb: FormBuilder,
  ) { }

  ngOnInit(): void { }

  private register(): void {
    this.formData.emit({
      type: this.fromType,
      formData: this.form.value
    });
  }

  public onSubmit(): void {
    this.register();
  }
}
