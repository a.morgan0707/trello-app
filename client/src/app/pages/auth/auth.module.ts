import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthComponent } from "./auth.component";
import { NzInputModule } from "ng-zorro-antd/input";
import { NzFormModule } from "ng-zorro-antd/form";
import { NzTabsModule } from "ng-zorro-antd/tabs";
import { NzButtonModule } from "ng-zorro-antd/button";
import { NzIconModule } from "ng-zorro-antd/icon";
import { IconDefinition } from "@ant-design/icons-angular";
import { CaretLeftOutline, SettingOutline, StepBackwardOutline } from "@ant-design/icons-angular/icons";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LoginFormComponent } from "./components/login-form/login-form.component";
import { RegisterFormComponent } from './components/register-form/register-form.component';
import { AuthService } from "./services/auth.service";
import { HttpClientModule } from "@angular/common/http";
import { AuthGuard } from "./guards/auth.guard";

const icons: IconDefinition[] = [
  StepBackwardOutline,
  CaretLeftOutline,
  SettingOutline
];

@NgModule({
  declarations: [
    AuthComponent,
    LoginFormComponent,
    RegisterFormComponent
  ],
  imports: [
    CommonModule,
    NzInputModule,
    NzFormModule,
    NzTabsModule,
    NzButtonModule,
    NzIconModule.forChild(icons),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    AuthService,
    AuthGuard
  ]
})
export class AuthModule { }
