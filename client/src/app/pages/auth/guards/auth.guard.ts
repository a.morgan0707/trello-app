import { Injectable } from "@angular/core";
import { CanActivate } from "@angular/router";
import { Observable } from "rxjs";
import { LocalstorageService } from "../../../shared/services/localstorage.service";

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private localstorageService: LocalstorageService
  ) {
  }

  canActivate(): Observable<boolean> | boolean {
    const token = this.localstorageService.get('token');

    return !!token;
  }
}
