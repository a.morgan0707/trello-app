import { Injectable } from "@angular/core";

@Injectable({ providedIn: 'root' })
export class LocalstorageService {
  constructor() { }

  public set(key: string, data: any): void {
    localStorage.setItem(key, JSON.stringify(data));
  }

  public get(key: string): string | null {
    const data = localStorage.getItem(key);

    return data ? JSON.parse(data) : null;
  }

  public remove(key: string): void {
    localStorage.removeItem(key);
  }

  public exist(key: string): boolean {
    const data = localStorage.getItem(key);

    return !!data;
  }
}
